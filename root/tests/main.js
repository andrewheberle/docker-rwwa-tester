const puppeteer = require('puppeteer');

const width = 1920;
const height = 1080;

const url = process.env.URL || 'http://invalid.url.local/';
const screenshotInterval = process.env.SCREENSHOT_INTERVAL || '5000';
const screenshotMax = process.env.SCREENSHOT_MAX || '1000';
const screenshotPath = process.env.SCREENSHOT_PATH || '/tmp';
const stbName = process.env.STB_NAME || 'undefined';
const stbBuild = process.env.STB_BUILD || '-1';
const stbMac = process.env.STB_MAC || '00:00:00:00:00:00';
const debugPort = process.env.DEBUG_PORT || '9090';
const jpegQuality = 80;

const corsHeaders = {
    'Access-Control-Allow-Origin': '*'
};

function pad(n, d) {
    if (d == 4) {
        if (n < 10) {
            return '000' + n;
        } else if (n < 100) {
            return '00' + n;
        } else if (n < 1000) {
            return '0' + n;
        } else {
            return n;
        }
    } else if (d == 3) {
        if (n < 10) {
            return '00' + n;
        } else if (n < 100) {
            return '0' + n;
        } else {
            return n;
        }
    } else if (d == 2) {
        if (n < 10) {
            return '0' + n;
        } else {
            return n;
        }
    } else {
        return n;
    }
}

function sleep(timeout) {
    return new Promise((resolve) => {
        setTimeout(resolve, timeout);
    });
}

(async () => {
        sleep(1000)
        try {
            const browser = await puppeteer.launch({
                    defaultViewport: {
                            width: width,
                            height: height
                    },
                    executablePath: '/usr/bin/google-chrome',
                    args: [
                            '--no-sandbox',
                            '--disable-setuid-sandbox',
                            '--disable-dev-shm-usage',
                            '--remote-debugging-address=0.0.0.0',
                            '--remote-debugging-port=' + debugPort,
                            '--bwsi',
                            '--start-maximized',
                            '--disable-restore-background-contents',
                            '--disable-confirmation',
                            '--disable-infobars',
                            '--disable-session-crashed-bubble',
                            '--fullscreen',
                            '--kiosk',
                            '--disk-cache-size=2000',
                            '--media-cache-size=2000',
                            '--aggressive-cache-discard',
                            '--disable-translate',
                            '--disable-hang-monitor'
                        ]
            });
            const page = await browser.newPage();
        
            await page.setRequestInterception(true);
            page.on('request', interceptedRequest => {
                    console.log(interceptedRequest.url());
                    switch (interceptedRequest.url()) {
                        case 'http://localhost/cgi-bin/get_mac.cgi':
                            interceptedRequest.respond({
                                status: 200,
                                contentType: 'text/html',
                                headers: corsHeaders,
                                body: stbMac
                            });
                            break;
                        case 'http://localhost/cgi-bin/get_rgi_card_os_build.cgi':
                            interceptedRequest.respond({
                                status: 200,
                                contentType: 'text/html',
                                headers: corsHeaders,
                                body: stbBuild
                            });
                            break;
                        case 'http://localhost/cgi-bin/play_video.cgi':
                            interceptedRequest.respond({
                                status: 200,
                                contentType: 'text/html',
                                headers: corsHeaders,
                                body: '<html><body><br></body></html>'
                            });
                            break;
                        default:
                            interceptedRequest.continue();
                    }
            });
            try {
                await page.goto(url);
                var i;
                for (i = 0; i < parseInt(screenshotMax); i++) {
                        try {
                            var d = new Date()
                            datestamp = d.getFullYear() + pad(d.getMonth(), 2) + pad(d.getDay(), 2) + pad(d.getHours(), 2) + pad(d.getMinutes(), 2) + pad(d.getSeconds(), 2)
                            await page.waitFor(parseInt(screenshotInterval));
                            await page.screenshot({path: screenshotPath + '/' + stbName + '-' + datestamp + '.jpg', type: 'jpeg', quality: jpegQuality});
                        } catch (e) {
                            console.log('An exception occurred during screenshot loop: ', e);
                            break;
                        }
                }
            } catch (e) {
                console.log('An exception occurred during page.goto: ', e);
            } finally {
                try {
                    await browser.close();
                } catch (e) {
                    console.log('An exception occurred during browser.close: ', e);
                } finally {
                    process.exit();
                }
            }
        } catch (e) {
            console.log('An exception occurred: ', e);
            process.exit();
        }
})();
