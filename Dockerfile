FROM node:11-slim

# Container arguments
ARG BUILD_DATE
ARG VERSION
ARG NAME="docker-rwwa-tester"
ARG DESCRIPTION="RWWA stress test container"
ARG AUTHOR="Andrew Heberle"
ARG VCS_BASE_URL="https://gitlab.com/andrewheberle"
ARG VCS_REF
ARG ARCH="amd64"

ENV URL="http://stbcontrol.agency.rwwa.local/" \
    SCREENSHOT_PATH="/tests/screenshots" \
    SCREENSHOT_MAX="1000" \
    SCREENSHOT_INTERVAL="5000" \
    STB_NAME="undefined" \
    STB_MAC="00:00:00:00:00:00" \
    STB_BUILD="-1" \
    DEBUG_PORT="9090" \
    CHROME_VERSION="72.0.3626.96"

RUN apt-get update -q && \
    apt-get install -yq libgconf-2-4 && \
    apt-get install -yq wget --no-install-recommends && \
    wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - && \
    sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' && \
    apt-get update -q && \
    apt-get install -yq google-chrome-unstable fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-kacst ttf-freefont && \
    apt-get install -yq google-chrome-stable=${CHROME_VERSION}-1 --no-install-recommends && \
    apt-get install -yq build-essential && \
    rm /etc/apt/sources.list.d/google-chrome.list && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get purge --auto-remove -yq curl && \
    rm -rf /src/*.deb

COPY root /

RUN npm install && \
    apt-get purge --auto-remove -y build-essential

CMD ["node", "/tests/main.js"]

LABEL name="$NAME" \
      maintainer="$AUTHOR ($VCS_BASE_URL/$NAME)" \
      version="$VERSION" \
      architecture="$ARCH" \
      description="$DESCRIPTION" \
      maintainer="$AUTHOR ($VCS_BASE_URL/$NAME)" \
      org.label-schema.description="$DESCRIPTION" \
      org.label-schema.build-date="$BUILD_DATE" \
      org.label-schema.name="$NAME" \
      org.label-schema.vcs-ref="$VCS_REF" \
      org.label-schema.vcs-url="$VCS_BASE_URL/$NAME" \
      org.label-schema.version="$VERSION" \
      org.label-schema.schema-version="1.0"
